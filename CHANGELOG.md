# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## v0.1.0 - 2020-04-11

### Added
- Initial release.
- Blog Post Properties: title, id, slug, post_date, excerpt, content, author_firstname, author_lastname, author_email, status, is_published, comment_count
- Optional Attributes: html, blog, section, format (post_date)


## v0.2.0 - 2020-04-12
### Added
- Post date prefixed to post title in selection.

### Fixed
- Fixed not able to deselect an item.

## v0.2.1 - 2020-04-12
### Fixed
- Fixed label text used to select empty item.

## v0.2.2 - 2020-04-13
### Fixed
- Fixed not being able to use perch:if exists within template on id of region.