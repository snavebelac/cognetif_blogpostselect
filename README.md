# Cognetif BlogPostSelect
Field type to select a blog post from the perch_blog list and return a property.

## Installation

### Recommended install via Git Submodules:
```
 $ git submodule add https://gitlab.com/cognetif-os/perch-dev/cognetif_blogpostselect.git perch/addons/fieldtypes/cognetif_blogpostselect 
```

### Alternative install via Zip Download:
1. Download zip archive and extract locally.
1. Create a `cognetif_blogpostselect` folder in the `/perch/addons/fieldtypes/` folder of your perch install.
1. Copy the `cognetif_blogpostselect.class.php` file to `/perch/addons/fieldtypes/cognetif_blogpostselect` folder 

## Usage
In a perch template, you can use the this field type as follows:
```xml
<perch:content id="some_post" type="cognetif_blogpostselect" prop="title" label="Select a blog post" />
```

### Available Properties:
- *title* - The post title
- *id* - The post internal ID
- *slug* - The post slug
- *post_date* - The post date
- *excerpt* - The post excerpt
- *content* - The post content
- *author_firstname* - The author first name
- *author_lastname* - The author last name
- *author_email* - The author email
- *status* - The post status [Published,Draft]
- *is_published* - Is the post published [0,1]
- *comment_count* - The number of comments

### Other attributes
- *section* - Blog section slug.  Limits posts in CMS selector to those associated to the blog section.
- *blog* - Blog slug. For use with multiple blogs, limits the posts in the CMS selector to those that belong to that blog.
- *html* - To return the html content rather than having the html tags encoded.
- *format* - Available on the `post_date` property to format the date using `strftime` function formatting.


# License
This project is free, open source, and GPL friendly. You can use it for commercial projects, open source projects, or really almost whatever you want.

# Donations
This is free software but it took some time to develop. If you use it, please send me a message I'd be interested to know which site uses it. If you appreciate the app and use it regularly, feel free to [Buy me a Beer](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=6EBCDCCZRNSWW&source=url)!

# Issues
Create a GitLab Issue: https://gitlab.com/cognetif-os/perch-dev/cognetif_blog-post-select/-/issues or better yet become a contributor.

Developer
Cognetif : Jordin Brown jbrown@cognetif.com