<?php

/**
 * A field type for selecting a blog post from the perch_blog list
 *
 * @package default
 * @author Jordin Brown
 */
class PerchFieldType_cognetif_blogpostselect extends PerchAPI_FieldType
{
    /** @var PerchBlog_Posts */
    private $factory;

    public function __construct(PerchForm $Form = null, PerchXMLTag $Tag = null, $app_id)
    {
        parent::__construct($Form, $Tag, $app_id);
        $this->factory = new PerchBlog_Posts();
    }

    /**
     * Form fields for the edit page
     *
     * @param array $details
     * @return string
     */
    public function render_inputs($details = array())
    {

        $id = $this->Tag->input_id();
        $val = '';

        //Repopulate with data stored for field (previous value).
        if (isset($details[$id]) && $details[$id] != '') {
            $json = $details[$id];
            $val = $json['postID'];
        }

        $posts = $this->selectData();

        // Create the field, or just output a dash if there are no results
        if (is_array($posts)) {
            $s = $this->Form->select($id, $posts, $val);
        } else {
            $s = '-';
        }

        // return a HTML string containing the form fields to display
        return $s;
    }


    /**
     * Read in the form input, prepare data for storage in the database.
     *
     * @param  $post
     * @param  $Item
     * @return array
     */
    public function get_raw($post = false, $Item = false)
    {
        $data = [];

        $id = $this->Tag->id();

        // $post should normally be set, but if it's not, try $_POST
        if ($post === false) {
            $post = $_POST;
        }

        // Find the data we need from the post
        if (isset($post[$id])) {
            $this->raw_item = trim($post[$id]);

            // Store the data as we want it
            $data['postID'] = $this->raw_item;
            $data['_default'] = $this->raw_item;
        }

        return $data;
    }

    /**
     * Take the raw data input and return process values for templating
     *
     * @param  $raw
     * @return string
     */
    public function get_processed($raw = false)
    {
        $content = false;

        // Check we've got the array we're expecting
        if (is_array($raw) && isset($raw['postID'])) {

            /** @var PerchBlog_Post $post */
            $post = $this->factory->find($raw['postID']);

            if ($post) {


                switch ($this->Tag->prop()) {

                    case 'title':
                        $content = $post->postTitle();
                        break;

                    case 'id':
                        $content = $post->postID();
                        break;

                    case 'slug':
                        $content = $post->postSlug();
                        break;

                    case 'post_date' :

                        $format = $this->Tag->format();
                        if (!$format) {
                            $format = '%d %b %Y';
                        }

                        try {
                            $date = new DateTime($post->postDateTime());
                            $content = strftime($format, $date->getTimestamp());

                        } catch (\Exception $e) {
                            PerchUtil::debug($e->getMessage(), 'error');
                            $content = '';
                        }
                        break;

                    case 'excerpt' :
                        $content = $post->excerpt();
                        break;

                    case 'content' :
                        $content = $post->postDescHTML();
                        break;
                    case 'author_firstname' :
                        $content = $post->get_author()->authorGivenName();
                        break;

                    case 'author_lastname' :
                        $content = $post->get_author()->authorFamilyName();
                        break;

                    case 'author_email' :
                        $content = $post->get_author()->authorEmail();
                        break;

                    case 'status' :
                        $content = $post->postStatus();
                        break;

                    case 'is_published' :
                        $content = $post->postIsPublished();
                        break;

                    case 'comment_count' :
                        $content = $post->postCommentCount();
                        break;

                    default:
                        $method = $this->Tag->prop();
                        if (method_exists($post, $method)) {
                            $content = $post->$method();
                        }
                        break;
                }
            }
        }

        if ($this->Tag->html()) {
            $content = html_entity_decode($content);
        }
        return $content;
    }

    /**
     * Get the value to be used for searching
     *
     * @param $raw
     * @return string
     */
    public function get_search_text($raw = false)
    {
        if ($raw === false) {
            $raw = $this->get_raw();
        }

        if (!PerchUtil::count($raw)) {
            return '';
        }

        /** @var PerchBlog_Post $post */
        $post = $this->factory->find($raw['postID']);

        if (!$post) {
            return '';
        }
        return $post->postTitle();
    }


    /**
     * Get the list of available posts
     *
     * @return array $files
     */
    private function selectData()
    {


        $blogSlug = $this->Tag->blog();
        $sectionSlug = $this->Tag->section();

        $options = [
            'template' => 'blog/post.html',
            'skip-template' => true,
            'sort' => 'postDateTime',
            'sort-order' => 'DESC',
            'filter' => []
        ];


        if ($blogSlug) {
            $blogFactory = new PerchBlog_Blogs();
            $blog = $blogFactory->get_one_by('blogSlug', $blogSlug);
            $options['filter'] = [
                ['filter' => 'blogID', 'match' => 'eq', 'value' => $blog->blogID()]
            ];
        }

        if ($sectionSlug) {

            $sectionFactory = new PerchBlog_Sections();
            $section = $sectionFactory->get_one_by('sectionSlug', $sectionSlug);

            if ($section) {
                $options['filter'] = [
                    ['filter' => 'sectionID', 'match' => 'eq', 'value' => $section->sectionId()]
                ];
            }
        }

        $posts = $this->factory->get_filtered_listing($options);

        $selectData = [
            ['label' => "Select a post", 'value' => '']
        ];

        if (PerchUtil::count($posts) > 0) {
            foreach ($posts as $post) {
                $selectData[] = array(
                    'label' => $post['postDateTime'] . ' - ' . $post['postTitle'],
                    'value' => $post['postID']
                );

            }
        }

        return $selectData;

    }

}
